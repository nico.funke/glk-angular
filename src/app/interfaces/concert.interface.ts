export interface Concert {
    readonly bands: string;
    readonly date: string;
    readonly place: string;
    readonly doors: string;
    readonly begin: string;
    readonly fblink: string;
    readonly buylink: string;
    picture: string;
    readonly description: string;
    readonly id?: string;
}
